/*
 * Navbar JS
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
function updateNavbar() {
    // Fetch user
    $.get('/user/user')
        .done(function (response) {
            $('#user').html(response);
        })
        .fail(function () {
            $('#user').html('Failed to load user data');
        });

    // Fetch language
    $.get('/user/language')
        .done(function (response) {
            $('#language').html(response);
        })
        .fail(function () {
            $('#language').html('Failed to load language data');
        });
}
