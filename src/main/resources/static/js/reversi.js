/*
 * General Java Scripts supporting 'single page' navigation.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
// variable section
let userEventChannel = false;
let loaded_url = "";

function updateDOM(target, response) {
    if (response.indexOf("<!DOCTYPE") !== -1) {
        document.open();
        document.write(response);
        document.close();
    } else {
        $(target).html(response);
    }
}

// default action after main page is loaded
$(document).ready(function () {
    $.get("/user/login", function (response) {
        updateDOM('#zero', response);
    });
});

// handle link navigation from container #zero
$(document).ready(function () {
    $('#zero').on('click', 'a[my-link]', function (e) {
        e.preventDefault();
        let url = $(this).attr('my-link');
        if (url == "return_url") {
            url = loaded_url;
        }
        $.get(url, function (response) {
            updateDOM('#zero', response);
        });
    })
});

// handle link navigation from user menu
$(document).ready(function () {
    $('#user').on('click', 'a[my-link]', function (e) {
        e.preventDefault();
        let url = $(this).attr('my-link');
        $.get(url, function (response) {
            updateDOM('#zero', response);
        });
    })
});

// handle language selection
$(document).ready(function () {
    $('#language').on('click', 'a[my-lang]', function (e) {
        const newLang = $(this).attr('my-lang');
        $.get('/user/language?lang=' + newLang, function (response) {
            updateDOM('#language', response);
        });
        $.get('/user/user?lang=' + newLang, function (response) {
            updateDOM('#user', response);
        });
        $.get(loaded_url + "?lang=" + newLang, function (response) {
            updateDOM('#zero', response);
        });
    })
});

// post forms
$(document).ready(function () {
    $('#zero').on('submit', 'form', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: $(this).serialize(),
            success: function (response) {
                updateDOM('#zero', response);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error('Error: ' + textStatus, errorThrown);
            }
        });
    });
});

// long pooling for events
async function eventChannel() {
    if(userEventChannel) {
        await fetch('/user/events').then(response => {
            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            return response;
        }).then(data => {
            if(userEventChannel) {
                if (data.length > 0) {
                    let alert = data.json();
                    showAlert(alert['type'], alert['header'], alert['text']);
                }
            }
        }).catch(error => {
            userEventChannel = false;
        }).finally(() => {
            setTimeout(eventChannel, 1000);
        });
    } else {
        setTimeout(eventChannel, 5000);
    }
};

// show alert in modal
function showAlert(aClass, aHeader, aText) {
    // select the alert DOM element
    var alert = document.querySelector('#alertModal .alert');
    var classes = Array.from(alert.classList);

    // remove classes matching pattern 'alert-*'
    classes.forEach(function(className) {
        if(className.startsWith('alert-')) {
            alert.classList.remove(className);
        }
    });

    alert.classList.add(aClass);
    alert.classList.add('alert-dismissible');
    $('#alertHeader').html(aHeader);
    $('#alertText').html(aText);
    $('#alertModal').modal('show');
}

function checkPassword() {
    var password = document.getElementById("password");
    var confirmation = document.getElementById("confirmation");

    if (password.value !== confirmation.value) {
        password.style.backgroundColor = "rgba(128, 0, 0, 0.25)";
        confirmation.style.backgroundColor = "rgba(128, 0, 0, 0.25)";
    } else {
        password.style.backgroundColor = "rgba(0, 0, 0, 0.75)";
        confirmation.style.backgroundColor = "rgba(0, 0, 0, 0.75)";
    }
}

function validateForm() {
    var password = document.getElementById("password").value;
    var confirmation = document.getElementById("confirmation").value;

    if (password !== confirmation) {
        return false;
    }
    return true;
}

function enableTooltips() {
    $('[data-bs-toggle="tooltip"]').tooltip();
};

$(document).ready(eventChannel());