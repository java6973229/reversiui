/*
 * Java Scripts related to game page.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
let boardEventChannel = false;
let moveEventChannel = false;

$('#zero').on('click', '.field', async function (e) {
    const col = getColIndex(e.target);
    const row = getRowIndex(e.target);
    console.log(`Element clicked. Column: ${col}, Row: ${row}`);

    try {
        const response = await makeMove(col, row);
    } catch (error) {
        console.error('Error making move:', error);
    }
});

function getColIndex(element) {
    return Array.from(element.parentNode.children).indexOf(element);
}

function getRowIndex(element) {
    return Array.from(element.parentNode.parentNode.children).indexOf(element.parentNode);
}

async function makeMove(col, row) {
    if (!moveEventChannel) return;

    const MOVE_URL = '/game/move/';
    moveEventChannel = false;

    try {
        const response = await fetch(MOVE_URL + col + '/' + row);
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
    } catch (error) {
        throw error;
    } finally {
        moveEventChannel = true;
    }
}

// long board update pooling
async function updateWait() {
    if (boardEventChannel) {
        try {
            await $.get('/game/board/wait', async function (response) {
                if (boardEventChannel && response.includes('field')) {
                    $('#board').html(response);
                    await updatePlayers();
                } else {
                    $('#zero').html(response);
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.error('Error fetching data: ', textStatus, errorThrown);
            });
        } catch (error) {
            console.error('An unexpected error occurred: ', error);
        } finally {
            setTimeout(updateWait, 500);
        }
    } else {
        setTimeout(updateWait, 2000);
    }
}

// immediate board update
function updateBoard() {
    $.get('/game/board/update').done(function (response) {
            if (response.includes('field')) {
                $('#board').html(response);
                updatePlayers();
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.error('Error fetching board: ', textStatus, errorThrown);
        });
}

// immediate players update
function updatePlayers() {
    $.get('/game/player/0').done(function (response) {
            $('#player-one').html(response);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.error('Error fetching player 0: ', textStatus, errorThrown);
        });
    $.get('/game/player/1').done(function (response) {
            $('#player-two').html(response);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.error('Error fetching player 1: ', textStatus, errorThrown);
        });
}

function waitForGame() {
    $.get('/game/board', function (response) {
        $('#zero').html(response);
    });
}

function updateTurnInfo() {
    let textFromTurnElement = $('#turn').attr('turn-text');
    $('#turn-banner').text(textFromTurnElement);
}

$(document).ready(updateWait());
$(document).ready(statusChannel());