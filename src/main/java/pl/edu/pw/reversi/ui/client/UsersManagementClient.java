/**
 * Client class for interacting with the users management API.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
package pl.edu.pw.reversi.ui.client;

import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import pl.edu.pw.reversi.ui.dto.AuthenticateUserRequest;
import pl.edu.pw.reversi.ui.dto.UserRequest;
import pl.edu.pw.reversi.ui.model.Users;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class UsersManagementClient implements IUsersManagementClient {
    private final RestTemplate restTemplate;
    private final ModelMapper modelMapper;
    @Value("${gameServer.ip}")
    private String ip;
    @Value("${gameServer.port}")
    private Integer port;
    @Value("${gameServer.ssl}")
    private Boolean ssl;
    private String baseURL;

    /**
     * Constructor for the UsersManagementClient class.
     *
     * @param modelMapper The ModelMapper object used for mapping entities.
     */
    public UsersManagementClient(ModelMapper modelMapper) {
        this.restTemplate = new RestTemplate();
        this.modelMapper = modelMapper;
    }

    /**
     * Initializes the base URL for the API based on the IP, port, and SSL configuration.
     */
    @PostConstruct
    public void init() {
        this.baseURL = (Boolean.TRUE.equals(ssl) ? "https://" : "http://") + ip + ":" + port + "/api/user";
    }

    /**
     * Registers a user by sending a POST request to the API.
     *
     * @param user The user object to be registered.
     * @return true if the registration is successful, false otherwise.
     */
    public Boolean registerUser(Users user) {
        try {
            UserRequest request = modelMapper.map(user, UserRequest.class);
            ResponseEntity<Boolean> response = restTemplate.postForEntity(baseURL + "/register", request, Boolean.class);
            return response.getBody();
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retrieves a user by sending a GET request to the API.
     *
     * @param email The email address of the user to retrieve.
     * @return The Users object representing the retrieved user, or null if an error occurs.
     */
    public Users getUser(String email) {
        try {
            ResponseEntity<Users> response = restTemplate.getForEntity(baseURL + "/get/" + email, Users.class);
            return response.getBody();
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retrieves all users by sending a GET request to the API.
     *
     * @return A list of Users objects representing all the retrieved users, or an empty list if an error occurs.
     */
    public List<Users> getAllUsers() {
        try {
            ResponseEntity<Users[]> response = restTemplate.getForEntity(baseURL + "/list", Users[].class);
            Users[] body = response.getBody();
            return body != null ? Arrays.asList(body) : Collections.emptyList();
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Updates a user by sending a PUT request to the API.
     *
     * @param user The user object to be updated.
     * @return true if the update is successful, false otherwise.
     */
    public Boolean updateUser(Users user) {
        try {
            HttpHeaders headers = new HttpHeaders();
            UserRequest request = modelMapper.map(user, UserRequest.class);
            ResponseEntity<Boolean> response = restTemplate.exchange(baseURL + "/update",
                    HttpMethod.PUT, new HttpEntity<>(request, headers), Boolean.class);
            return response.getBody();
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Unregisters a user by sending a DELETE request to the API.
     *
     * @param userId The ID of the user to be unregistered.
     * @return true if the unregistration is successful, false otherwise.
     */
    public Boolean unregisterUser(Long userId) {
        try {
            HttpHeaders headers = new HttpHeaders();
            ResponseEntity<Boolean> response = restTemplate.exchange(baseURL + "/unregister/" + userId, HttpMethod.DELETE,
                    new HttpEntity<>(null, headers), Boolean.class);
            return response.getBody();
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Authenticates a user by sending a POST request to the API.
     *
     * @param email           The email address of the user.
     * @param hashedPassword The hashed password of the user.
     * @return true if the authentication is successful, false otherwise.
     */
    public Boolean authenticateUser(String email, String hashedPassword) {
        try {
            AuthenticateUserRequest request = new AuthenticateUserRequest();
            request.setEmail(email);
            request.setHashedPassword(hashedPassword);
            ResponseEntity<Boolean> response = restTemplate.postForEntity(baseURL + "/authenticate", request, Boolean.class);
            return response.getBody();
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Logs out a user by sending a DELETE request to the API.
     *
     * @param userId The ID of the user to be logged out.
     * @return true if the logout is successful, false otherwise.
     */
    public Boolean logoutUser(Long userId) {
        try {
            HttpHeaders headers = new HttpHeaders();
            ResponseEntity<Boolean> response = restTemplate.exchange(baseURL + "/logout/" + userId, HttpMethod.DELETE,
                    new HttpEntity<>(null, headers), Boolean.class);
            return response.getBody();
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }
}
