/**
 * The UserRequest class represents the data associated with a user.
 * This class is typically used for creating or updating user information.
 * It contains the user's ID, email, name, and hashed password.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
package pl.edu.pw.reversi.ui.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRequest {
    private Long userId;
    private String email;
    private String name;
    private String hashedPassword;
}
