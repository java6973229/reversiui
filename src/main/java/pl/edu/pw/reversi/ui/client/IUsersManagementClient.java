/**
 * Interface for the UsersManagementClient class.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
package pl.edu.pw.reversi.ui.client;

import pl.edu.pw.reversi.ui.model.Users;

import java.util.List;

public interface IUsersManagementClient {
    /**
     * Authenticates a user.
     *
     * @param email    The email address of the user.
     * @param password The password of the user.
     * @return true if the authentication is successful, false otherwise.
     */
    Boolean authenticateUser(String email, String password);

    /**
     * Logs out a user.
     *
     * @param userId The ID of the user to be logged out.
     * @return true if the logout is successful, false otherwise.
     */
    Boolean logoutUser(Long userId);

    /**
     * Registers a user.
     *
     * @param user The user object to be registered.
     * @return true if the registration is successful, false otherwise.
     */
    Boolean registerUser(Users user);

    /**
     * Updates a user.
     *
     * @param user The user object to be updated.
     * @return true if the update is successful, false otherwise.
     */
    Boolean updateUser(Users user);

    /**
     * Retrieves a user by email.
     *
     * @param email The email address of the user to retrieve.
     * @return The Users object representing the retrieved user, or null if not found.
     */
    Users getUser(String email);

    /**
     * Retrieves all users.
     *
     * @return A list of Users objects representing all the retrieved users, or an empty list if none found.
     */
    List<Users> getAllUsers();

    /**
     * Unregisters a user.
     *
     * @param userId The ID of the user to be unregistered.
     * @return true if the unregistration is successful, false otherwise.
     */
    Boolean unregisterUser(Long userId);
}
