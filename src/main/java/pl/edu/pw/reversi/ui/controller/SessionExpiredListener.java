/**
 * Listener class for handling session expiration events.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
package pl.edu.pw.reversi.ui.controller;

import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.web.session.HttpSessionDestroyedEvent;
import org.springframework.stereotype.Component;
import pl.edu.pw.reversi.ui.service.IUserInterfaceService;

@Component
public class SessionExpiredListener implements ApplicationListener<HttpSessionDestroyedEvent> {
    private static final String USER_ID = "user_id";
    private final IUserInterfaceService usersManagementService;

    /**
     * Constructor for the SessionExpiredListener class.
     *
     * @param usersManagementService User interface service for managing users.
     */
    @Autowired
    public SessionExpiredListener(IUserInterfaceService usersManagementService) {
        this.usersManagementService = usersManagementService;
    }

    /**
     * Handles the HttpSessionDestroyedEvent when a session expires.
     *
     * @param event The HttpSessionDestroyedEvent object.
     */
    @Override
    public void onApplicationEvent(HttpSessionDestroyedEvent event) {
        HttpSession session = event.getSession();
        if (session != null) {
            Long userId = (Long) session.getAttribute(USER_ID);
            if (userId != null) {
                usersManagementService.logoutUser(userId);
            }
        }
    }
}