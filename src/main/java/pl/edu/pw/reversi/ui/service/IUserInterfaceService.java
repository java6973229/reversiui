/**
 * Interface for user interface service.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
package pl.edu.pw.reversi.ui.service;

import pl.edu.pw.reversi.ui.model.Users;

import java.util.List;

public interface IUserInterfaceService {

    /**
     * Authenticates a user.
     *
     * @param email    The email of the user.
     * @param password The password of the user.
     * @return true if the user is authenticated, false otherwise.
     */
    Boolean authenticateUser(String email, String password);

    /**
     * Logs out a user.
     *
     * @param userId The ID of the user.
     * @return true if the user is logged out successfully, false otherwise.
     */
    Boolean logoutUser(Long userId);

    /**
     * Registers a new user.
     *
     * @param username The username of the new user.
     * @param email    The email of the new user.
     * @param password The password of the new user.
     * @return true if the user is registered successfully, false otherwise.
     */
    Boolean registerUser(String username, String email, String password);

    /**
     * Updates a user.
     *
     * @param userId   The ID of the user to update.
     * @param username The updated username.
     * @param password The updated password.
     * @return true if the user is updated successfully, false otherwise.
     */
    Boolean updateUser(Long userId, String username, String password);

    /**
     * Retrieves a user by email.
     *
     * @param email The email of the user.
     * @return The Users object representing the user, or null if not found.
     */
    Users getUser(String email);

    /**
     * Retrieves a list of all users.
     *
     * @return The list of Users objects representing all users.
     */
    List<Users> getUsers();

    /**
     * Unregisters a user.
     *
     * @param userId The ID of the user to unregister.
     * @return true if the user is unregistered successfully, false otherwise.
     */
    Boolean unregisterUser(Long userId);
}
