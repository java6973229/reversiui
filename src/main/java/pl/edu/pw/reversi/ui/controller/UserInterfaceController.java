/**
 * Controller class for the user interface.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
package pl.edu.pw.reversi.ui.controller;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.edu.pw.reversi.gi.model.Alert;
import pl.edu.pw.reversi.gi.service.IGameInterfaceService;
import pl.edu.pw.reversi.ui.model.Users;
import pl.edu.pw.reversi.ui.service.IUserInterfaceService;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/user")
public class UserInterfaceController {
    private static final String USER_ID = "user_id";
    private static final String USERNAME = "username";
    private static final String EMAIL = "email";
    private static final String PASSWORD = "password";
    private static final String GAME_ID = "game_id";
    private static final String LANG = "lang";
    private static final String ALERT = "alert";
    private final IUserInterfaceService usersManagementService;
    private final IGameInterfaceService gameInterfaceService;

    /**
     * Constructor for the UserInterfaceController class.
     *
     * @param usersManagementService User interface service for managing users.
     * @param gameInterfaceService   Game interface service.
     */
    @Autowired
    public UserInterfaceController(
            IUserInterfaceService usersManagementService,
            IGameInterfaceService gameInterfaceService
    ) {
        this.usersManagementService = usersManagementService;
        this.gameInterfaceService = gameInterfaceService;
    }

    /**
     * Handler for the index page.
     *
     * @return The name of the index page view.
     */
    @GetMapping(path = "/")
    public String indexPage() {
        return "index";
    }

    /**
     * Handler for the login page.
     *
     * @param model   The model to be used in the view.
     * @param session The HttpSession object.
     * @return The name of the login page view or a redirection to the lobby page if the user is already logged in.
     */
    @GetMapping("/login")
    public String loginPage(Model model, HttpSession session) {
        if (session != null && session.getAttribute(EMAIL) != null) {
            return "redirect:/user/lobby";
        } else {
            model.addAttribute(ALERT, "");
            return "fragments/ui/login_page";
        }
    }

    /**
     * Handler for the login form submission.
     *
     * @param email    The user's email address.
     * @param password The user's password.
     * @param model    The model to be used in the view.
     * @param session  The HttpSession object.
     * @return A redirection to the lobby page if the user is authenticated, or the login page with a warning message.
     */
    @PostMapping("/login")
    public String loginForm(@RequestParam(EMAIL) String email,
                            @RequestParam(PASSWORD) String password,
                            Model model, HttpSession session) {
        Boolean isAuthenticated = usersManagementService.authenticateUser(email, password);
        if (isAuthenticated != null) {
            if (Boolean.TRUE.equals(isAuthenticated)) {
                Users user = usersManagementService.getUser(email);
                ArrayBlockingQueue<Alert> queue = new ArrayBlockingQueue<>(10);
                session.setAttribute(USER_ID, user.getUserId());
                session.setAttribute(USERNAME, user.getName());
                session.setAttribute(EMAIL, user.getEmail());
                session.setAttribute("queue", queue);
                return "redirect:/user/lobby";
            }
            model.addAttribute(ALERT, "warning");
            return "fragments/ui/login_page";
        } else {
            return "fragments/ui/error";
        }
    }

    /**
     * Handler for the logout page.
     *
     * @param session The HttpSession object.
     * @return A redirection to the login page after logging out the user.
     */
    @GetMapping("/logout")
    public String logoutPage(HttpSession session) {
        Long userId = (Long) session.getAttribute(USER_ID);
        if (usersManagementService.logoutUser(userId) != null) {
            session.invalidate();
            return "redirect:/user/login";
        } else {
            return "fragments/ui/error";
        }
    }

    /**
     * Handler for the lobby page.
     *
     * @param session The HttpSession object.
     * @return A redirection to the game creation page if a game ID is available, or the lobby page otherwise.
     */
    @GetMapping("/lobby")
    public String lobbyPage(HttpSession session) {
        Long userId = (Long) session.getAttribute(USER_ID);
        Long gameId = (Long) session.getAttribute(GAME_ID);
        if (gameId == null && userId != null) {
            gameId = gameInterfaceService.getGameId(userId);
        }
        if (gameId != null) {
            session.setAttribute(GAME_ID, gameId);
            return "redirect:/game/create";
        }
        return "fragments/ui/lobby_page";
    }

    /**
     * Handler for the user dropdown menu.
     *
     * @param model   The model to be used in the view.
     * @param session The HttpSession object.
     * @return The name of the user dropdown menu view.
     */
    @GetMapping("/user")
    public String user(Model model, HttpSession session) {
        Object user = session.getAttribute(USERNAME);
        if (user != null) {
            model.addAttribute(USERNAME, user.toString());
        } else {
            model.addAttribute(USERNAME, "");
        }
        return "fragments/ui/user_dropdown";
    }

    /**
     * Handler for the hall of fame page.
     *
     * @param model The model to be used in the view.
     * @return The name of the hall of fame page view or an error page if an error occurs.
     */
    @GetMapping("/hall")
    public String hall(Model model) {
        List<Users> users = usersManagementService.getUsers();
        if (users != null) {
            model.addAttribute("users", users);
            return "fragments/ui/hall_of_fame_page";
        } else {
            return "fragments/ui/error";
        }
    }

    /**
     * Handler for the language dropdown menu.
     *
     * @param model   The model to be used in the view.
     * @param request The HttpServletRequest object.
     * @return The name of the language dropdown menu view.
     */
    @GetMapping("/language")
    public String language(Model model, HttpServletRequest request) {
        String lang = request.getParameter(LANG);
        if (lang == null) {
            Cookie[] cookies = request.getCookies();
            lang = "us";
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if ("org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE".equals(cookie.getName())) {
                        lang = cookie.getValue();
                        break;
                    }
                }
            }
        }
        model.addAttribute("lang", lang);
        return "fragments/ui/language_dropdown";
    }

    /**
     * Handler for the registration page.
     *
     * @param model The model to be used in the view.
     * @return The name of the registration page view.
     */
    @GetMapping("/register")
    public String registerPage(Model model) {
        model.addAttribute(ALERT, "");
        return "fragments/ui/register_page";
    }

    /**
     * Handler for the registration form submission.
     *
     * @param username The user's username.
     * @param email    The user's email address.
     * @param password The user's password.
     * @param model    The model to be used in the view.
     * @return The name of the registration page view with a success or warning message, or an error page if an error occurs.
     */
    @PostMapping("/register")
    public String registerForm(
            @RequestParam(USERNAME) String username,
            @RequestParam(EMAIL) String email,
            @RequestParam(PASSWORD) String password,
            Model model) {
        Boolean isRegistered = usersManagementService.registerUser(username, email, password);
        if (isRegistered != null) {
            String alert;
            if (Boolean.TRUE.equals(isRegistered)) {
                alert = "success";
            } else {
                alert = "warning";
            }
            model.addAttribute(ALERT, alert);
            model.addAttribute(USERNAME, username);
            model.addAttribute(EMAIL, email);
            return "fragments/ui/register_page";
        } else {
            return "fragments/ui/error";
        }
    }

    /**
     * Handler for the settings page.
     *
     * @param model   The model to be used in the view.
     * @param session The HttpSession object.
     * @return The name of the settings page view or an error page if an error occurs.
     */
    @GetMapping("/update")
    public String settingsPage(Model model, HttpSession session) {
        if (session != null) {
            model.addAttribute(EMAIL, session.getAttribute(EMAIL));
            model.addAttribute(USERNAME, session.getAttribute(USERNAME));
            model.addAttribute(ALERT, "");
            return "fragments/ui/profile_update_page";
        } else {
            return "fragments/ui/error";
        }
    }

    /**
     * Handler for the settings form submission.
     *
     * @param username The user's updated username.
     * @param password The user's updated password.
     * @param model    The model to be used in the view.
     * @param session  The HttpSession object.
     * @return The name of the settings page view with a success or warning message, or an error page if an error occurs.
     */
    @PostMapping("/update")
    public String settingsForm(
            @RequestParam(USERNAME) String username,
            @RequestParam(PASSWORD) String password,
            Model model, HttpSession session
    ) {
        Long userId = Long.valueOf(session.getAttribute(USER_ID).toString());
        Boolean isUpdated = usersManagementService.updateUser(userId, username, password);
        if (isUpdated != null) {
            String alert;
            if (Boolean.TRUE.equals(isUpdated)) {
                session.setAttribute(USERNAME, username);
                alert = "success";
            } else {
                alert = "warning";
            }
            model.addAttribute(USERNAME, session.getAttribute(USERNAME));
            model.addAttribute(ALERT, alert);
            return "fragments/ui/profile_update_page";
        } else {
            return "fragments/ui/error";
        }
    }

    /**
     * Handler for the user unregistering.
     *
     * @param session The HttpSession object.
     * @return A redirection to the login page after unregistering the user, or an error page if an error occurs.
     */
    @GetMapping("/unregister")
    public String unregister(HttpSession session) {
        Long userId = Long.valueOf(session.getAttribute(USER_ID).toString());
        if (usersManagementService.unregisterUser(userId) != null) {
            session.invalidate();
            return "redirect:/user/login";
        } else {
            return "fragments/ui/error";
        }
    }

    /**
     * Handler for the error page.
     *
     * @param session The HttpSession object.
     * @return The name of the error page view.
     */
    @GetMapping(path = "/error")
    public String error(HttpSession session) {
        session.invalidate();
        return "error";
    }

    /**
     * Handler for retrieving events from the event queue.
     *
     * @param session The HttpSession object.
     * @return A ResponseEntity containing an Alert object from the event queue, or null if the queue is empty.
     */
    @GetMapping("/events")
    @ResponseBody
    public ResponseEntity<Alert> getEvents(HttpSession session) {
        Object object = session.getAttribute("queue");
        if (object instanceof ArrayBlockingQueue) {
            ArrayBlockingQueue<Alert> queue = (ArrayBlockingQueue<Alert>) object;
            try {
                return ResponseEntity.ok(queue.poll(5, TimeUnit.SECONDS));
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new RuntimeException(e);
            }
        }
        return ResponseEntity.ok(null);
    }
}
