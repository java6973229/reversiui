/**
 * The AuthenticateUserRequest class represents the data necessary for user authentication.
 * It contains the user's email and the hashed version of the user's password.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
package pl.edu.pw.reversi.ui.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthenticateUserRequest {
    private String email;
    private String hashedPassword;
}
