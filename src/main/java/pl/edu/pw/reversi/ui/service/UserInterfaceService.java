/**
 * Service class for user interface operations.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
package pl.edu.pw.reversi.ui.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pw.reversi.ui.client.IUsersManagementClient;
import pl.edu.pw.reversi.ui.model.Password;
import pl.edu.pw.reversi.ui.model.Users;

import java.util.List;

@Service
public class UserInterfaceService implements IUserInterfaceService {

    private final IUsersManagementClient usersManagementClient;

    @Autowired
    public UserInterfaceService(IUsersManagementClient usersManagementClient) {
        this.usersManagementClient = usersManagementClient;
    }

    /**
     * Authenticates a user.
     *
     * @param email    The email of the user.
     * @param password The password of the user.
     * @return true if the user is authenticated, false otherwise.
     */
    public Boolean authenticateUser(String email, String password) {
        if (email != null && password != null && !email.isBlank() && !password.isBlank()) {
            return usersManagementClient.authenticateUser(email, Password.hashPassword(password));
        }
        return Boolean.FALSE;
    }

    /**
     * Logs out a user.
     *
     * @param userId The ID of the user.
     * @return true if the user is logged out successfully, false otherwise.
     */
    public Boolean logoutUser(Long userId) {
        if (userId != null && userId > 0L) {
            return usersManagementClient.logoutUser(userId);
        }

        return Boolean.FALSE;
    }

    /**
     * Retrieves a user by email.
     *
     * @param email The email of the user.
     * @return The Users object representing the user, or null if not found.
     */
    public Users getUser(String email) {
        if (email != null && !email.isBlank()) {
            return usersManagementClient.getUser(email);
        }

        return null;
    }

    /**
     * Retrieves a list of all users.
     *
     * @return The list of Users objects representing all users.
     */
    public List<Users> getUsers() {
        return usersManagementClient.getAllUsers();
    }

    /**
     * Registers a new user.
     *
     * @param username The username of the new user.
     * @param email    The email of the new user.
     * @param password The password of the new user.
     * @return true if the user is registered successfully, false otherwise.
     */
    public Boolean registerUser(String username, String email, String password) {
        if (username != null && !username.isBlank() && email != null && !email.isBlank()
                && password != null && !password.isBlank()) {
            // TODO: add code that would check input data with regex
            Users user = new Users();
            user.setName(username);
            user.setEmail(email);
            user.setHashedPassword(Password.hashPassword(password));
            return usersManagementClient.registerUser(user);
        }
        return Boolean.FALSE;
    }

    /**
     * Updates a user.
     *
     * @param userId   The ID of the user to update.
     * @param username The updated username.
     * @param password The updated password.
     * @return true if the user is updated successfully, false otherwise.
     */
    public Boolean updateUser(Long userId, String username, String password) {
        if (userId != null && userId > 0L && username != null && !username.isBlank()) {
            // TODO: add code that would check input data with regex
            Users user = new Users();
            user.setUserId(userId);
            user.setName(username);
            user.setHashedPassword(Password.hashPassword(password));
            return usersManagementClient.updateUser(user);
        }
        return Boolean.FALSE;
    }

    /**
     * Unregisters a user.
     *
     * @param userId The ID of the user to unregister.
     * @return true if the user is unregistered successfully, false otherwise.
     */
    public Boolean unregisterUser(Long userId) {
        if (userId != null && userId > 0L) {
            return usersManagementClient.unregisterUser(userId);
        }
        return Boolean.FALSE;
    }
}
