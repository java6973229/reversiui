/**
 * Filter class for controlling access to web server resources and end-points.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
package pl.edu.pw.reversi.ui.controller;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Component
public class FilterController extends OncePerRequestFilter {

    private static final List<String> OPEN_URIS = Arrays.asList("/user/login", "/css/", "/music/", "/images/", "/js/",
            "/favicon.ico", "/user/user", "/user/language", "/user/register", "/error");

    /**
     * Performs the actual filtering of the request.
     *
     * @param request     The HttpServletRequest object.
     * @param response    The HttpServletResponse object.
     * @param filterChain The FilterChain object.
     * @throws ServletException If a servlet exception occurs.
     * @throws IOException      If an I/O exception occurs.
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        if (session != null && session.getAttribute("email") == null) {
            String uri = request.getRequestURI();
            boolean isOpenUri = OPEN_URIS.stream().anyMatch(uri::startsWith);

            if (!isOpenUri && !uri.equals("/")) {
                response.sendRedirect("/");
                return;
            }
        }
        filterChain.doFilter(request, response);
    }
}
