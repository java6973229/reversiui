/**
 * Represents a User entity within the application.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
package pl.edu.pw.reversi.ui.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Users {

    private Long userId;

    private String email;

    private String name;

    private String hashedPassword;

    private Long totalPoints;

    private LocalDateTime registrationDate;

    private LocalDateTime modificationDate;
}
