/**
 * The UserResponse class represents the data that is sent as a response
 * after processing a user request. It contains the user's ID, email, name,
 * and total points which might represent some form of score or reputation.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
package pl.edu.pw.reversi.ui.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserResponse {
    private Long userId;
    private String email;
    private String name;
    private Long totalPoints;
}
