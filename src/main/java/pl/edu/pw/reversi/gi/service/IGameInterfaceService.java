/**
 * Interface for the GameInterfaceService, which provides operations related to game management.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
package pl.edu.pw.reversi.gi.service;

import pl.edu.pw.reversi.gi.model.*;

import java.util.List;

public interface IGameInterfaceService {
    /**
     * Creates a new game session with the specified player.
     *
     * @param playerId The ID of the player.
     * @return The ID of the created game session.
     */
    Long createGame(Long playerId);

    /**
     * Allows a player to join a game session.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player.
     * @return true if the player successfully joins the game, false otherwise.
     */
    Boolean joinGame(Long gameId, Long playerId);

    /**
     * Allows a player to leave a game session.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player.
     */
    void leaveGame(Long gameId, Long playerId);

    /**
     * Retrieves a list of game sessions.
     *
     * @return A list of GameSession objects representing the available game sessions.
     */
    List<GameSession> gameList();

    /**
     * Retrieves the board of a game session.
     *
     * @param gameId The ID of the game session.
     * @return The Board object representing the game board.
     */
    Board getBoard(Long gameId);

    /**
     * Retrieves the player with the specified index in a game session.
     *
     * @param gameId The ID of the game session.
     * @param index  The index of the player.
     * @return The Player object representing the player.
     */
    Player getPlayer(Long gameId, Integer index);

    /**
     * Retrieves the status of a game session.
     *
     * @param gameId The ID of the game session.
     * @return The GameSessionStatus object representing the status of the game session.
     */
    GameSessionStatus getGameSessionStatus(Long gameId);

    /**
     * Retrieves the ID of the game session in which the player is participating.
     *
     * @param playerId The ID of the player.
     * @return The ID of the game session, or null if the player is not in any game session.
     */
    Long getGameId(Long playerId);

    /**
     * Makes a move in a game session.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player.
     * @param x        The X-coordinate of the move.
     * @param y        The Y-coordinate of the move.
     */
    void makeMove(Long gameId, Long playerId, Integer x, Integer y);

    /**
     * Waits for the player's move in a game session.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player.
     * @return true if the player's move is received, false if the waiting times out.
     */
    Boolean waitForMove(Long gameId, Long playerId);

    /**
     * Waits for the game to be ready to start in a game session.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player.
     * @return true if the game is ready to start, false if the waiting times out.
     */
    Boolean waitForGame(Long gameId, Long playerId);

    /**
     * Retrieves the result of a game session for a player.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player.
     * @return The GameResult object representing the result of the game for the player.
     */
    GameResult getGameResult(Long gameId, Long playerId);

    /**
     * Checks if the game was started or time-outed on waiting.
     *
     * @param gameId   The ID of the game session.
     * @return True is 2nd player joined, False otherwise.
     */
    Boolean wasGameStarted(Long gameId);
}
