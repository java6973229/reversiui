/**
 * Controller class that handles game-related requests and actions.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gi.controller;

import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.edu.pw.reversi.gi.model.*;
import pl.edu.pw.reversi.gi.service.IGameInterfaceService;

import java.util.List;

@Controller
@RequestMapping("/game")
public class GameInterfaceController {
    private final IGameInterfaceService gameInterfaceService;
    private static final String USER_ID = "user_id";
    private static final String GAME_ID = "game_id";

    /**
     * Constructor for the GameInterfaceController class.
     *
     * @param gameInterfaceService The service used for game interface operations.
     */
    @Autowired
    public GameInterfaceController(IGameInterfaceService gameInterfaceService) {
        this.gameInterfaceService = gameInterfaceService;
    }

    /**
     * Handles the "create" endpoint to create a new game session.
     *
     * @param session The HttpSession object representing the user session.
     * @return The view name to be rendered.
     */
    @GetMapping("/create")
    public String create(HttpSession session) {
        Long gameId = (Long) session.getAttribute(GAME_ID);
        Long userId = (Long) session.getAttribute(USER_ID);
        try {
            if (gameId == null) {
                gameId = gameInterfaceService.createGame(userId);
                session.setAttribute(GAME_ID, gameId);
                return "fragments/gi/waiting_for_game_page";
            } else {
                if (gameInterfaceService.getGameSessionStatus(gameId)
                        .equals(GameSessionStatus.ACTIVE)) {
                    return "fragments/gi/game_page";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "fragments/gi/waiting_for_game_page";
    }

    /**
     * Handles the "join" endpoint to join a game session.
     *
     * @param gameId The ID of the game session to join.
     * @param model  The Model object for adding attributes to the view.
     * @param session The HttpSession object representing the user session.
     * @return The view name to be rendered.
     */
    @GetMapping("/join/{gameId}")
    public String join(@PathVariable Long gameId, Model model, HttpSession session) {
        Long userId = (Long) session.getAttribute(USER_ID);
        try {
            if(Boolean.TRUE.equals(gameInterfaceService.joinGame(gameId, userId))) {
                session.setAttribute(GAME_ID, gameId);
                Board board = gameInterfaceService.getBoard(gameId);
                model.addAttribute("board", board.getBoard());
                return "fragments/gi/game_page";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/user/lobby";
    }

    /**
     * Handles the "list" endpoint to retrieve a list of available game sessions.
     *
     * @param model The Model object for adding attributes to the view.
     * @return The view name to be rendered.
     */
    @GetMapping("/list")
    public String list(Model model) {
        try {
            List<GameSession> gameSessions = gameInterfaceService.gameList();
            model.addAttribute("games", gameSessions);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "fragments/gi/game_list_page";
    }

    /**
     * Handles the "board" endpoint to display the game board.
     *
     * @param session The HttpSession object representing the user session.
     * @return The view name to be rendered.
     */
    @GetMapping("/board")
    public String board(HttpSession session) {
        Long gameId = (Long) session.getAttribute(GAME_ID);
        Long userId = (Long) session.getAttribute(USER_ID);
        try {
            if (gameId != null && userId != null) {
                if(gameInterfaceService.waitForGame(gameId, userId) != null) {
                    return "fragments/gi/game_page";
                } else {
                    return "fragments/ui/error";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/";
    }

    /**
     * Handles the "board/update" endpoint to update the game board.
     *
     * @param model   The Model object for adding attributes to the view.
     * @param session The HttpSession object representing the user session.
     * @return The view name to be rendered.
     */
    @GetMapping("/board/update")
    public String boardUpdate(Model model, HttpSession session) {
        Long gameId = (Long) session.getAttribute(GAME_ID);
        try {
            if (gameId != null) {
                Board board = gameInterfaceService.getBoard(gameId);
                if(board != null) {
                    model.addAttribute("board", board.getBoard());
                    return "fragments/gi/board_update";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "fragments/ui/error";
    }

    /**
     * Handles the "board/wait" endpoint to wait for the next move on the game board.
     *
     * @param model   The Model object for adding attributes to the view.
     * @param session The HttpSession object representing the user session.
     * @return The view name to be rendered.
     */
    @GetMapping("/board/wait")
    public String boardWait(Model model, HttpSession session) {
        Long gameId = (Long) session.getAttribute(GAME_ID);
        Long userId = (Long) session.getAttribute(USER_ID);
        try {
            if (gameId != null && userId != null) {
                if (gameInterfaceService.waitForMove(gameId, userId) != null) {
                    Board board = gameInterfaceService.getBoard(gameId);
                    if (board != null) {
                        GameSessionStatus status = gameInterfaceService.getGameSessionStatus(gameId);
                        if (status != null && status.equals(GameSessionStatus.ACTIVE)) {
                            model.addAttribute("board", board.getBoard());
                            return "fragments/gi/board_update";
                        } else {
                            Boolean wasGameStarted = gameInterfaceService.wasGameStarted(gameId);
                            if (wasGameStarted != null && !wasGameStarted) {
                                session.removeAttribute(GAME_ID);
                                return "redirect:/user/lobby";
                            }
                            return "redirect:/game/over";
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "fragments/ui/error";
    }

    /**
     * Handles the "player/{index}" endpoint to update the player information.
     *
     * @param index   The index of the player.
     * @param model   The Model object for adding attributes to the view.
     * @param session The HttpSession object representing the user session.
     * @return The view name to be rendered.
     */
    @GetMapping("/player/{index}")
    public String boardUpdate(@PathVariable Integer index, Model model, HttpSession session) {
        Long gameId = (Long) session.getAttribute(GAME_ID);
        try {
            if (gameId != null) {
                Player player = gameInterfaceService.getPlayer(gameId, index);
                model.addAttribute("player", player);
                return "fragments/gi/player_update";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "fragments/gi/dummy";
    }

    /**
     * Handles the "move/{x}/{y}" endpoint to make a move on the game board.
     *
     * @param x       The X-coordinate of the move.
     * @param y       The Y-coordinate of the move.
     * @param session The HttpSession object representing the user session.
     * @return The ResponseEntity object representing the HTTP response.
     */
    @GetMapping("/move/{x}/{y}")
    @ResponseBody
    public ResponseEntity<Void> move(@PathVariable Integer x, @PathVariable Integer y, HttpSession session) {
        Long userId = (Long) session.getAttribute(USER_ID);
        Long gameId = (Long) session.getAttribute(GAME_ID);
        try {
            gameInterfaceService.makeMove(gameId, userId, x, y);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Handles the "leave" endpoint to leave the game session.
     *
     * @param model   The Model object for adding attributes to the view.
     * @param session The HttpSession object representing the user session.
     * @return The view name to be rendered.
     */
    @GetMapping("/leave")
    public String leave(Model model, HttpSession session) {
        Long userId = (Long) session.getAttribute(USER_ID);
        Long gameId = (Long) session.getAttribute(GAME_ID);
        gameInterfaceService.leaveGame(gameId, userId);
        session.removeAttribute(GAME_ID);
        session.setAttribute("leave", gameId);
        Boolean wasGameStarted = gameInterfaceService.wasGameStarted(gameId);
        if (wasGameStarted != null && !wasGameStarted) {
            return "redirect:/user/lobby";
        }
        GameResult result = gameInterfaceService.getGameResult(gameId, userId);
        if (result != null) {
            model.addAttribute("result", result);
            return "fragments/gi/game_over_page";
        }
        return "fragments/ui/error";
    }

    /**
     * Handles the "over" endpoint when the game is over.
     *
     * @param model   The Model object for adding attributes to the view.
     * @param session The HttpSession object representing the user session.
     * @return The view name to be rendered.
     */
    @GetMapping("/over")
    public String over(Model model, HttpSession session) {
        Long userId = (Long) session.getAttribute(USER_ID);
        Long gameId = (Long) session.getAttribute(GAME_ID);
        if (gameId == null) {
            gameId = (Long) session.getAttribute("leave");
            session.removeAttribute("leave");
        }
        session.removeAttribute(GAME_ID);
        GameResult result = gameInterfaceService.getGameResult(gameId, userId);
        if (result != null) {
            model.addAttribute("result", result);
            return "fragments/gi/game_over_page";
        }
        return "fragments/ui/error";
    }

    /**
     * Handles the "invalidate" endpoint to invalidate the game session.
     *
     * @param session The HttpSession object representing the user session.
     * @return An empty string.
     */
    @GetMapping("/invalidate")
    @ResponseBody
    public String invalidate(HttpSession session) {
        session.removeAttribute(GAME_ID);
        return "";
    }

    /**
     * Handles the "status" endpoint to retrieve the status of the game session.
     *
     * @param session The HttpSession object representing the user session.
     * @return The ResponseEntity object representing the HTTP response containing the game session status.
     */
    @GetMapping("/status")
    @ResponseBody
    public ResponseEntity<GameSessionStatus> getEvents(HttpSession session) {
        Long gameId = (Long) session.getAttribute(GAME_ID);
        if (gameId != null) {
            GameSessionStatus status = gameInterfaceService.getGameSessionStatus(gameId);
            return ResponseEntity.ok(status);
        } else {
            return ResponseEntity.ok(null);
        }
    }
}
