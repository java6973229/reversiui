/**
 * This class represents a player in the game session.
 * It stores information about the player's ID, name, color, score, remaining pawns, and activity status.
 * The player's information can be updated based on the game status.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gi.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Player {
    Long id;
    String name;
    PawnColors color;
    Integer score;
    Integer remainingPawns;
    Boolean isActive;
}