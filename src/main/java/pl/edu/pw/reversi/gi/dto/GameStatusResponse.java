/**
 * Represents a response containing the game session status.
 * Contains the game session status.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gi.dto;

import lombok.Getter;
import lombok.Setter;
import pl.edu.pw.reversi.gi.model.GameSessionStatus;

@Getter
@Setter
public class GameStatusResponse {
    private GameSessionStatus gameSessionStatus; // The game session status
}
