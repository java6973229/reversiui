/**
 * Represents the game board for Reversi.
 * The board is represented as a 2D grid of integer values.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gi.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Board {
    private List<List<Integer>> board;
}
