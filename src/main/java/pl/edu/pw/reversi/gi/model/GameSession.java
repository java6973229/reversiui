/**
 * Represents a game session.
 * It manages the game state, players, and interactions with the game server.
 * Provides methods to make moves, create/join/leave games, and handle game termination.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
public class GameSession {
    private Long gameId;
    private Board board;
    private List<Player> players;
    private GameSessionStatus gameSessionStatus;
    private LocalDateTime creationDate;
}