/**
 * The GameSessionClient class is responsible for making HTTP requests to the game server's API
 * related to game sessions. It provides methods to create game sessions, retrieve game lists,
 * get game session status, join and leave games, get game boards and players, make moves,
 * and retrieve game results.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gi.client;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import pl.edu.pw.reversi.gi.dto.*;
import pl.edu.pw.reversi.gi.model.*;

import java.util.ArrayList;
import java.util.List;

@Component
public class GameSessionClient implements IGameSessionClient {

    private final RestTemplate restTemplate;

    @Value("${gameServer.ip:'127.0.0.1'}")
    private String ip;

    @Value("${gameServer.port:8443}")
    private Integer port;

    @Value("${gameServer.ssl:false}")
    private Boolean ssl;

    private String baseURL;

    /**
     * Constructs a new GameSessionClient with a RestTemplate instance.
     */
    public GameSessionClient() {
        this.restTemplate = new RestTemplate();
    }

    /**
     * Initializes the baseURL using the configured IP, port, and SSL settings.
     */
    @PostConstruct
    public void init() {
        this.baseURL = (Boolean.TRUE.equals(ssl) ? "https://" : "http://") + ip + ":" + port + "/api/game";
    }

    /**
     * Creates a new game session for the specified playerId.
     *
     * @param playerId the ID of the player creating the game session
     * @return the ID of the created game session, or null if an error occurred
     */
    public Long createGame(Long playerId) {
        try {
            ResponseEntity<CreateGameResponse> response = restTemplate.postForEntity(
                    baseURL + "/create/" + playerId.toString(), null, CreateGameResponse.class);
            CreateGameResponse body = response.getBody();
            return body != null ? body.getGameId() : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retrieves a list of game sessions.
     *
     * @return the list of game sessions, or an empty list if an error occurred
     */
    public List<GameSession> gameList() {
        try {
            ResponseEntity<GameListResponse> response = restTemplate.getForEntity(baseURL + "/list",
                    GameListResponse.class);
            GameListResponse body = response.getBody();
            return body != null ? body.getGameSessions() : null;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    /**
     * Retrieves the status of the specified game session.
     *
     * @param gameId the ID of the game session
     * @return the status of the game session, or null if an error occurred
     */
    public GameSessionStatus getGameSessionStatus(Long gameId) {
        try {
            ResponseEntity<GameStatusResponse> response = restTemplate.getForEntity(
                    baseURL + "/status/" + gameId, GameStatusResponse.class);
            GameStatusResponse body = response.getBody();
            return body != null ? body.getGameSessionStatus() : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retrieves the game ID associated with the specified player ID.
     *
     * @param playerId the ID of the player
     * @return the ID of the game associated with the player, or null if an error occurred
     */
    public Long getGameId(Long playerId) {
        try {
            ResponseEntity<Long> response = restTemplate.getForEntity(baseURL + "/game/" + playerId, Long.class);
            return response.getBody();
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Joins the specified game session with the specified player.
     *
     * @param gameId   the ID of the game session to join
     * @param playerId the ID of the player joining the game
     * @return the ID of the joined game session, or null if an error occurred
     */
    public Long joinGame(Long gameId, Long playerId) {
        try {
            HttpHeaders headers = new HttpHeaders();
            JoinGameRequest request = new JoinGameRequest();
            request.setPlayerId(playerId);
            ResponseEntity<JoinGameResponse> response = restTemplate.exchange(baseURL + "/join/" + gameId,
                    HttpMethod.PUT, new HttpEntity<>(request, headers), JoinGameResponse.class);
            JoinGameResponse body = response.getBody();
            return body != null ? body.getGameId() : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Leaves the specified game session for the specified player.
     *
     * @param gameId   the ID of the game session to leave
     * @param playerId the ID of the player leaving the game
     */
    public void leaveGame(Long gameId, Long playerId) {
        try {
            restTemplate.exchange(baseURL + "/leave/" + gameId + "/" + playerId, HttpMethod.DELETE, null,
                    Boolean.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves the board of the specified game session.
     *
     * @param gameId the ID of the game session
     * @return the board of the game session, or null if an error occurred
     */
    public Board getBoard(Long gameId) {
        try {
            ResponseEntity<GetBoardResponse> response = restTemplate.getForEntity(
                    baseURL + "/board/" + gameId, GetBoardResponse.class);
            GetBoardResponse body = response.getBody();
            if (body == null) {
                return null;
            }
            Board board = new Board();
            board.setBoard(body.getBoard());
            return board;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retrieves the player with the specified index in the specified game session.
     *
     * @param gameId the ID of the game session
     * @param index  the index of the player
     * @return the player with the specified index, or null if an error occurred
     */
    public Player getPlayer(Long gameId, Integer index) {
        try {
            ResponseEntity<GetPlayerResponse> response = restTemplate.getForEntity(
                    baseURL + "/player/" + gameId + "/" + index, GetPlayerResponse.class);
            GetPlayerResponse body = response.getBody();
            return body != null ? body.getPlayer() : null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Makes a move in the specified game session for the specified player at the specified position.
     *
     * @param gameId   the ID of the game session
     * @param playerId the ID of the player making the move
     * @param x        the x-coordinate of the move position
     * @param y        the y-coordinate of the move position
     */
    public void makeMove(Long gameId, Long playerId, Integer x, Integer y) {
        try {
            HttpHeaders headers = new HttpHeaders();
            MoveRequest moveRequest = new MoveRequest();
            moveRequest.setX(x);
            moveRequest.setY(y);
            HttpEntity<MoveRequest> request = new HttpEntity<>(moveRequest, headers);
            restTemplate.exchange(baseURL + "/move/" + gameId + "/" + playerId, HttpMethod.PUT, request, Void.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Waits for a move in the specified game session for the specified player.
     *
     * @param gameId   the ID of the game session
     * @param playerId the ID of the player
     * @return true if a move is available, false if an error occurred, or null if waiting times out
     */
    public Boolean waitForMove(Long gameId, Long playerId) {
        try {
            restTemplate.getForEntity(baseURL + "/wait/" + gameId + "/" + playerId, Void.class);
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Waits for the specified game session to start for the specified player.
     *
     * @param gameId   the ID of the game session
     * @param playerId the ID of the player
     * @return true if the game session starts, false if an error occurred, or null if waiting times out
     */
    public Boolean waitForGame(Long gameId, Long playerId) {
        try {
            restTemplate.getForEntity(baseURL + "/start/" + gameId + "/" + playerId, Void.class);
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retrieves the game result for the specified game session and player.
     *
     * @param gameId   the ID of the game session
     * @param playerId the ID of the player
     * @return the game result, or null if an error occurred
     */
    public GameResult getGameResult(Long gameId, Long playerId) {
        try {
            ResponseEntity<GameResult> response = restTemplate.getForEntity(
                    baseURL + "/result/" + gameId + "/" + playerId, GameResult.class);
            return response.getBody();
        } catch (RestClientException e) {
            e.printStackTrace();
            return null;
        }
    }
}
