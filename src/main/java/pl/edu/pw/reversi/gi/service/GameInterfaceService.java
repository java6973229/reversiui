/**
 * Service class that provides operations related to game session management.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
package pl.edu.pw.reversi.gi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pw.reversi.gi.client.IGameSessionClient;
import pl.edu.pw.reversi.gi.model.*;

import java.util.List;

@Service
public class GameInterfaceService implements IGameInterfaceService {

    private final IGameSessionClient gameSessionClient;
    private static final Integer MIN_INDEX = 0;
    private static final Integer MAX_INDEX = 1;
    private static final Integer MIN_X = 0;
    private static final Integer MAX_X = 7;
    private static final Integer MIN_Y = 0;
    private static final Integer MAX_Y = 7;

    /**
     * Constructor for the GameInterfaceService class.
     *
     * @param gameSessionClient The client used for interacting with game sessions.
     */
    @Autowired
    public GameInterfaceService(IGameSessionClient gameSessionClient) {
        this.gameSessionClient = gameSessionClient;
    }

    /**
     * Creates a new game session with the specified player.
     *
     * @param playerId The ID of the player.
     * @return The ID of the created game session.
     */
    public Long createGame(Long playerId) {
        if (playerId != null && playerId > 0L) {
            return gameSessionClient.createGame(playerId);
        }
        return null;
    }

    /**
     * Allows a player to join a game session.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player.
     * @return true if the player successfully joins the game, false otherwise.
     */
    public Boolean joinGame(Long gameId, Long playerId) {
        if (gameId != null && gameId > 0L
                && playerId != null && playerId > 0L) {
            GameSessionStatus status = gameSessionClient.getGameSessionStatus(gameId);
            if (status.equals(GameSessionStatus.NEW)) {
                gameSessionClient.joinGame(gameId, playerId);
                return Boolean.TRUE;
            } else {
                return Boolean.FALSE;
            }
        }
        return Boolean.FALSE;
    }

    /**
     * Allows a player to leave a game session.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player.
     */
    public void leaveGame(Long gameId, Long playerId) {
        if (gameId != null && gameId > 0L
                && playerId != null && playerId > 0L) {
            gameSessionClient.leaveGame(gameId, playerId);
        }
    }

    /**
     * Retrieves a list of game sessions.
     *
     * @return A list of GameSession objects representing the available game sessions.
     */
    public List<GameSession> gameList() {
        return gameSessionClient.gameList();
    }

    /**
     * Retrieves the board of a game session.
     *
     * @param gameId The ID of the game session.
     * @return The Board object representing the game board.
     */
    public Board getBoard(Long gameId) {
        if (gameId != null && gameId > 0L) {
            return gameSessionClient.getBoard(gameId);
        }
        return null;
    }

    /**
     * Retrieves the player with the specified index in a game session.
     *
     * @param gameId The ID of the game session.
     * @param index  The index of the player.
     * @return The Player object representing the player.
     */
    public Player getPlayer(Long gameId, Integer index) {
        if (gameId != null && gameId > 0L && index != null
                && index >= MIN_INDEX && index <= MAX_INDEX) {
            return gameSessionClient.getPlayer(gameId, index);
        }
        return null;
    }

    /**
     * Retrieves the status of a game session.
     *
     * @param gameId The ID of the game session.
     * @return The GameSessionStatus object representing the status of the game session.
     */
    public GameSessionStatus getGameSessionStatus(Long gameId) {
        if (gameId != null && gameId > 0L) {
            return gameSessionClient.getGameSessionStatus(gameId);
        }
        return null;
    }

    /**
     * Retrieves the ID of the game session in which the player is participating.
     *
     * @param playerId The ID of the player.
     * @return The ID of the game session, or null if the player is not in any game session.
     */
    public Long getGameId(Long playerId) {
        if (playerId != null && playerId > 0L) {
            return gameSessionClient.getGameId(playerId);
        }
        return null;
    }

    /**
     * Makes a move in a game session.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player.
     * @param x        The X-coordinate of the move.
     * @param y        The Y-coordinate of the move.
     */
    public void makeMove(Long gameId, Long playerId, Integer x, Integer y) {
        if (gameId != null && gameId > 0L && playerId != null && playerId > 0L
                && x != null && x >= MIN_X && x <= MAX_X
                && y != null && y >= MIN_Y && y <= MAX_Y) {
            gameSessionClient.makeMove(gameId, playerId, x, y);
        }
    }

    /**
     * Waits for the player's move in a game session.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player.
     * @return true if the player's move is received, false if the waiting times out.
     */
    public Boolean waitForMove(Long gameId, Long playerId) {
        if (gameId != null && gameId > 0L && playerId != null && playerId > 0L) {
            return gameSessionClient.waitForMove(gameId, playerId);
        }
        return null;
    }

    /**
     * Waits for the game to be ready to start in a game session.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player.
     * @return true if the game is ready to start, false if the waiting times out.
     */
    public Boolean waitForGame(Long gameId, Long playerId) {
        if (gameId != null && gameId > 0L && playerId != null && playerId > 0L) {
            return gameSessionClient.waitForGame(gameId, playerId);
        }
        return null;
    }

    /**
     * Retrieves the result of a game session for a player.
     *
     * @param gameId   The ID of the game session.
     * @param playerId The ID of the player.
     * @return The GameResult object representing the result of the game for the player.
     */
    public GameResult getGameResult(Long gameId, Long playerId) {
        if (gameId != null && gameId > 0L && playerId != null && playerId > 0L) {
            return gameSessionClient.getGameResult(gameId, playerId);
        }
        return null;
    }

    /**
     * Checks if the game was started or time-outed on waiting.
     *
     * @param gameId   The ID of the game session.
     * @return True is 2nd player joined, False otherwise.
     */
    public Boolean wasGameStarted(Long gameId) {
        if (gameId != null) {
            Player player;
            for (int index = 0; index < 2; index++) {
                player = gameSessionClient.getPlayer(gameId, index);
                if (player != null) {
                    if (player.getName().equals("?")) {
                        return Boolean.FALSE;
                    }
                } else {
                    return null;
                }
            }
            return Boolean.TRUE;
        }
        return null;
    }
}
