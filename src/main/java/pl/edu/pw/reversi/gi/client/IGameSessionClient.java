/**
 * This interface represents a client for interacting with a game session.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gi.client;

import pl.edu.pw.reversi.gi.model.*;

import java.util.List;

public interface IGameSessionClient {
    /**
     * Creates a new game session with the specified player.
     *
     * @param playerId the ID of the player creating the game session
     * @return the ID of the created game session
     */
    Long createGame(Long playerId);

    /**
     * Joins the specified game session as a player.
     *
     * @param gameId   the ID of the game session to join
     * @param playerId the ID of the player joining the game session
     * @return the ID of the joined game session
     */
    Long joinGame(Long gameId, Long playerId);

    /**
     * Leaves the specified game session as a player.
     *
     * @param gameId   the ID of the game session to leave
     * @param playerId the ID of the player leaving the game session
     */
    void leaveGame(Long gameId, Long playerId);

    /**
     * Retrieves a list of all available game sessions.
     *
     * @return a list of GameSession objects representing the available game sessions
     */
    List<GameSession> gameList();

    /**
     * Retrieves the status of the specified game session.
     *
     * @param gameId the ID of the game session to retrieve the status of
     * @return the GameSessionStatus of the specified game session
     */
    GameSessionStatus getGameSessionStatus(Long gameId);

    /**
     * Retrieves the ID of the game session associated with the specified player.
     *
     * @param playerId the ID of the player
     * @return the ID of the game session associated with the player
     */
    Long getGameId(Long playerId);

    /**
     * Retrieves the board of the specified game session.
     *
     * @param gameId the ID of the game session
     * @return the Board object representing the game board
     */
    Board getBoard(Long gameId);

    /**
     * Retrieves the player at the specified index in the game session.
     *
     * @param gameId the ID of the game session
     * @param index  the index of the player to retrieve
     * @return the Player object representing the player at the specified index
     */
    Player getPlayer(Long gameId, Integer index);

    /**
     * Makes a move in the specified game session.
     *
     * @param gameId   the ID of the game session
     * @param playerId the ID of the player making the move
     * @param x        the x-coordinate of the move
     * @param y        the y-coordinate of the move
     */
    void makeMove(Long gameId, Long playerId, Integer x, Integer y);

    /**
     * Waits for a move to be made in the specified game session by the specified player.
     *
     * @param gameId   the ID of the game session to wait for the move in
     * @param playerId the ID of the player to wait for the move from
     * @return true if a move was made by the specified player, false otherwise
     */
    Boolean waitForMove(Long gameId, Long playerId);

    /**
     * Waits for the specified game session to be in progress.
     *
     * @param gameId   the ID of the game session to wait for
     * @param playerId the ID of the player waiting for the game session
     * @return true if the game session is in progress, false otherwise
     */
    Boolean waitForGame(Long gameId, Long playerId);

    /**
     * Retrieves the result of the specified game session for the specified player.
     *
     * @param gameId   the ID of the game session
     * @param playerId the ID of the player
     * @return the GameResult of the specified game session for the specified player
     */
    GameResult getGameResult(Long gameId, Long playerId);
}

