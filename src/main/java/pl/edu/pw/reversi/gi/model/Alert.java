/**
 * Model class representing an alert message.
 *
 * @author Damian Pasik
 * @version 0.9
 * @since 11.06.2023
 */
package pl.edu.pw.reversi.gi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Alert {
    private String type;
    private String header;
    private String text;
}
