/**
 * The main class for the Reversi User Interface application.
 * This class is responsible for configuring and running the application.
 *
 * @author  Damian Pasik
 * @version 0.9
 * @since   11.06.2023
 */
package pl.edu.pw.reversi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

@SpringBootApplication
public class ReversiUserInterfaceApplication implements WebMvcConfigurer {

    private final LocaleChangeInterceptor localeChangeInterceptor;

    /**
     * Constructs a new ReversiUserInterfaceApplication object with the given {@link LocaleChangeInterceptor}.
     *
     * @param localeChangeInterceptor The locale change interceptor to be used by the application.
     */
    public ReversiUserInterfaceApplication(LocaleChangeInterceptor localeChangeInterceptor) {
        this.localeChangeInterceptor = localeChangeInterceptor;
    }

    /**
     * Adds the locale change interceptor to the interceptor registry.
     *
     * @param registry The interceptor registry to which the locale change interceptor is added.
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor);
    }

    /**
     * The main method that starts the Reversi User Interface application.
     *
     * @param args The command-line arguments passed to the application.
     */
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ReversiUserInterfaceApplication.class);
        app.addListeners((ApplicationListener<ApplicationEnvironmentPreparedEvent>) event -> {
            Environment env = event.getEnvironment();
            System.setProperty("server.port", env.getProperty("localPort", "80"));
            System.setProperty("server.ssl.enabled", env.getProperty("localSSL", "false"));
            System.setProperty("gameServer.ip", env.getProperty("remoteIp", "127.0.0.1"));
            System.setProperty("gameServer.port", env.getProperty("remotePort", "8080"));
            System.setProperty("gameServer.ssl", env.getProperty("remoteSSL", "false"));
        });
        app.run(args);
    }
}
