# JEE (2023L) Projekt końcowy
# Space Reversi

### Rejestr zmian

**16.06.2023**

- z klasy UserManagementApplication usunięto zbędne atrybuty
- w klasie GameSessionService poprawiono metodę addPointsToUser - dodano weryfikację czy getUserById zwraca wartość null oraz czy ilość punktów do dodania jest większa od zera
- w klasie GameInterfaceService dodano metodę wasGameStarted, która sprawdza czy gra została rozpoczęta czy zokończyła się przed rozpoczęciem wskutek przekroczenia czasu oczekiwania
- z pliku game_page.js usunięto metodę implementującą pooling statusu gry - status gry jest teraz sprawdzanu po ruchu, podczas aktualizacji planszy
- w belce nawigacyjnej dodano przycisk 'i', który wyświetla informację o programie

### Informacja o dostarczonych plikach

- **ReversiGS-0.0.1-SNAPSHOT.jar** – Serwer gry Space Reversi, implementujący logikę gry.
- **ReversiUM-0.0.1-SNAPSHOT.jar** – Serwer zarządzający bazą użytkowników oraz sesjami gry; korzysta z usług serwera reversiServerSide.
- **ReversiUI-0.0.1-SNAPSHOT.jar** – Serwer webowy dostarczający interfejs użytkownika; korzysta z usług serwera ReversiUM.
- **ReversiUM.zip** – Kod źródłowy serwera ReversiUM.
- **ReversiUI.zip** – Kod źródłowy serwera ReversiUI.
- **Test_Results_ReversiUM.zip** – Raport z testów integracyjnych i coverage.
- **reversi_navigation.zip** – Raport z testów aplikacji wykonanych przy pomocy RobotFramework + SeleniumLibrary.
- **reversi_gameplay.zip** - Raport z testów przebiegu gry wykonanych przy pomocy RobotFramework + SeleniumLibrary.

### Instrukcja uruchomienia aplikacji

- **Wymagania**: 
  - Java 17+
  - Wolne porty TCP/IP: 80, 8080, 8090
  - Zalecane użycie na PC z ekranem FullHD w poziomie
  - Przeglądarka: FF, Chrome, Edge

- **Uwagi**:
  - `ReversiGS-0.0.1-SNAPSHOT.jar` używa portu 8090 (hardcoded), jest serwerem rest-api dla 'ReversiUM-0.0.1-SNAPSHOT.jar`.
  - `ReversiUM-0.0.1-SNAPSHOT.jar` używa portu 8080 (domyślnie), jest serwerem rest-api dla `ReversiUI-0.0.1-SNAPSHOT.jar`.
  - `ReversiUI-0.0.1-SNAPSHOT.jar` używa portu 80 (domyślnie), jest web serwerem.
  - `ReversiUM` i `ReversiUI` mogą być uruchomione z parametrami:
    - `localPort=<numer_portu>` – port na którym zostanie uruchomiony serwer.
    - `localSSL=<true/false>` - włączenie SSL dla graczy (domyślnie false; SSL może być włączony na ReversiUI).
	- `remoteIp=<numer_portu>` - adres IP serwera
	- `remotePort=<numer_portu>` - numer portu, na którym serwer nasłuchuje
	- `remoteSSL=<true/false>` - włączenie SSL na połączeniu z serwerem (komunikacja SSL pomiędzy ReversiUI i ReversiUM jest możliwa ale wymaga instalacji certyfikatu serwera w środowisku uruchomieniowym Java; `reversiServerSide` nie wspiera SSL)

- **Uruchomienie z domyślnymi parametrami**:
  1. Skopiuj pliki *.jar do katalogu z uprawnieniami R/W (przy starcie zostanie utworzony plik bazy danych `reversi.mv.db`).
  2. Uruchom serwisy gry w następujący sposób:
     ```
     java -jar ReversiGS-0.0.1-SNAPSHOT.jar
     java -jar ReversiUM-0.0.1-SNAPSHOT.jar
     java -jar ReversiUI-0.0.1-SNAPSHOT.jar
     ```

### Instrukcja użytkowania

1. W przeglądarce WWW otwórz adres `http(s)://<IP>:<PORT>`, gdzie `<IP>` odpowiada adresowi komputera, na którym ReversiUI został uruchomiony, a `<PORT>` odpowiada numerowi portu, na którym ReversiUI nasłuchuje (domyślnie 80, więc można pominąć).
2. Aby zagrać w grę, należy się zalogować. Można skorzystać w tym celu z predefiniowanych użytkowników (hasło jest takie samo dla wszystkich: `2Sec4You`):
    - neil.armstrong@example.com
    - yuri.gagarin@example.com
    - sally.ride@example.com
    - chris.hadfield@example.com
3. Po zalogowaniu dwóch użytkowników, jednym należy stworzyć grę, a drugim do niej dołączyć.
4. Informacja o tym, kto ma ruch, jest wyświetlona nad planszą. Należy naprzemiennie wybierać pola pulsujące na zielono. Gra kończy się gdy:
   - plansza zostanie zapełniona,
   - gracz nie ma możliwości ruchu,
   - któryś z graczy opuści grę,
   - nastąpi timeout: 60s na wykonanie ruchu, 120s na rozpoczęcie gry.
5. Wynik każdej rozgrywki jest automatycznie dodany do sumarycznych punktów gracza.

### Autorzy
- **ReversiGS**: Gaworczyk Bernard
- **ReversiUI**, **ReversiUM**: Pasik Damian
